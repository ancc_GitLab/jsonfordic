//
//  HandleOF_NULL.h
//  JsonforDic
//
//  Created by ma huajian on 14-9-2.
//  Copyright (c) 2014年 ancc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HandleOF_NULL : NSObject
/**
 * 太久以前写的代码,忘了是做什么用的,应该是消除json中隐藏的null值
 *
 * @param con json串
 *
 * @return 转化后的json串,这里的转化是指消除null值
 */
+(id)getCorrectedObject:(id)con;
@end
