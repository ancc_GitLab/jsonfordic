//
//  main.m
//  JsonforDic
//
//  Created by ma huajian on 14-9-2.
//  Copyright (c) 2014年 ancc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HandleOF_NULL.h"
#import "HandleOF_JSON.h"
int main(int argc, const char * argv[])
{
    //输入EOF结束
    @autoreleasepool
    {
        printf(" - - - - -请输入json串,输入EOF开始整理json串- - - - - - \n");
        char str[10];
        NSMutableString *s=[[NSMutableString alloc]initWithCapacity:1024*1024];
        while (1)
        {
            scanf("%s",str);
            //检测输入的字符,对C语言不熟悉,只做了简单的字符串对比判断
            if(!strncmp(str, "EOF", 3)||!strncmp(str, "eof", 3))
                break;
            [s appendString:[[NSString alloc]initWithUTF8String:str]];
            //            if(s.length>3&&[s hasSuffix:@"\n"]&&[[s substringToIndex:s.length-1]hasSuffix:@"\n"])
            //                break;
        }
        if(![s hasPrefix:@"{"])
            [s insertString:@"{" atIndex:0];
        if(![s hasSuffix:@"}"])
            [s appendString:@"}"];
        //将输入的json字符串转换成data格式
        NSData *utf8data=[s dataUsingEncoding:NSUTF8StringEncoding];
        //转换成id类型的json格式,这一步完成了只要转化的主要工作
        id json=[NSJSONSerialization JSONObjectWithData:utf8data options:0 error:nil];
        //规范化一点,为了后续调用!
        NSDictionary *dic =[[NSDictionary alloc]initWithDictionary:json];
        //消除json可能包含的null值
        dic=[HandleOF_NULL getCorrectedObject:dic];
        if(dic.allKeys.count==0)
        {
            printf(" - - - - -json格式有误,请重新运行- - - - - - \n");
            exit(0);
        }
        printf(" - - - - -json正常,请输入类名- - - - - - \n");
        scanf("%s",str);
        NSString *className=[[NSString alloc]initWithUTF8String:str];
        printf(" - - - - -整理后的数据实体类- - - - - - \n");
        //整理格式
        NSString *bean=[HandleOF_JSON getBeanData:className data:dic];
        NSLog(@"\n%@",bean);
        printf(" - - - - -出错难免- - - - - - \n");
        
        
    }
    return 0;
}



