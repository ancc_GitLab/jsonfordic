//
//  HandleOF_NULL.m
//  JsonforDic
//
//  Created by ma huajian on 14-9-2.
//  Copyright (c) 2014年 ancc. All rights reserved.
//

#import "HandleOF_NULL.h"

@implementation HandleOF_NULL
#pragma mark 处理数据中可能存在的"null"
+(NSString*)getCorrectedValue:(id)inputValue
{
    NSString *correctedString = @"";
    if (inputValue && (NSNull *)inputValue != [NSNull null]) {
        correctedString = (NSString*)inputValue;
    }
    if(![correctedString isKindOfClass:NSString.class]&&
       ![correctedString isKindOfClass:NSDictionary.class]&&
       ![correctedString isKindOfClass:NSArray.class])
    {
        correctedString=[NSString stringWithFormat:@"%@",correctedString];
    }
    return correctedString;
}

+(NSDictionary *)getCorrectedDic:(NSDictionary *)dic
{
    NSMutableDictionary *tempDic=[NSMutableDictionary new];
    for(NSString *key in dic.allKeys)
    {
        id con=[self.class getCorrectedValue:[dic objectForKey:key]];
        if([con isKindOfClass:NSDictionary.class])
            con=[self.class getCorrectedDic:con];
        if([con isKindOfClass:NSArray.class])
            con=[self.class getCorrectedArr:con];
        [tempDic setObject:con forKey:key];
    }
    return tempDic;
}

+(NSArray *)getCorrectedArr:(NSArray *)arr
{
    NSMutableArray *tempArr=[NSMutableArray new];
    for(int i=0;i<arr.count;i++)
    {
        id con =arr[i];
        con=[self.class getCorrectedValue:con];
        if([con isKindOfClass:NSDictionary.class])
            con=[self.class getCorrectedDic:con];
        if([con isKindOfClass:NSArray.class])
            con=[self.class getCorrectedArr:con];
        [tempArr addObject:con];
    }
    return tempArr;
}

+(id)getCorrectedObject:(id)con
{
    con=[self.class getCorrectedValue:con];
    if([con isKindOfClass:NSDictionary.class])
        con=[self.class getCorrectedDic:con];
    if([con isKindOfClass:NSArray.class])
        con=[self.class getCorrectedArr:con];
    return con;
}
@end
