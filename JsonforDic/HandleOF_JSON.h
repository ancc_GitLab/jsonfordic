//
//  HandleOF_JSON.h
//  JsonforDic
//
//  Created by ma huajian on 14-9-2.
//  Copyright (c) 2014年 ancc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HandleOF_JSON : NSObject
/**
 * 将输入的字典转换成数据实体类
 *
 * @param dic 待转换的字典数据
 *
 * @return 数据实体类,字符串样式
 */
+(NSString *)getBeanData:(NSString *)className data:(NSDictionary*)dic;
@end
