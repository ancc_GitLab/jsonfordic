//
//  HandleOF_JSON.m
//  JsonforDic
//
//  Created by ma huajian on 14-9-2.
//  Copyright (c) 2014年 ancc. All rights reserved.
//

#import "HandleOF_JSON.h"

typedef enum {
    JSON_KEY_STYLE_Defaults,
    JSON_KEY_STYLE_NSString,
    JSON_KEY_STYLE_NSInteger,
    JSON_KEY_STYLE_CGFloat,
    JSON_KEY_STYLE_BOOL,
    JSON_KEY_STYLE_NSArray,
    JSON_KEY_STYLE_NSDictionary
}JSON_KEY_STYLE;
@implementation HandleOF_JSON
+(NSString *)getBeanData:(NSString *)className data:(NSDictionary*)dic
{
    if([dic.allKeys containsObject:@"Success"]&&[dic.allKeys containsObject:@"Token"])
    {
        id aid=dic[@"Data"];
        if(aid&&[aid isKindOfClass:NSDictionary.class])
            dic=aid;
        else{
            NSLog(@"没有data数据");
            return nil;
        }
    }
    NSMutableString *att_h=[NSMutableString new];
    NSMutableString *att_m=[NSMutableString new];
    [att_h appendString:@"/***/\n"];
    [att_m appendString:@"/***/\n"];
    if(![className hasPrefix:@"DATA_"])
    {
        className=[@"DATA_" stringByAppendingString:className];
        [att_h appendFormat:@"@interface %@ : WE_ResBaseData\n",className];
    }else{
        [att_h appendFormat:@"@interface %@ : WE_BaseData\n",className];
    }
    [att_m appendFormat:@"@implementation %@\n",className];
    for(NSString *key in dic.allKeys)
    {
        id value=[dic objectForKey:key];
        JSON_KEY_STYLE style=[self checkStyle:value];
        [att_h appendFormat:@"/***/\n%@\n",[self attribute:style t:key]];
        NSString *subClassName=[NSString stringWithFormat:@"%@_%@",className,[self toLowerCaseString:key]];
        [att_m appendFormat:@"/***/\n%@\n",[self example:style t:key c:subClassName]];
    }
    [att_h appendString:@"@end"];
    [att_m appendString:@"@end"];
    
    NSMutableString *rootAtt=[NSMutableString new];
    [rootAtt appendFormat:@"%@\n\n%@\n\n\n",att_h,att_m];
    
    //子类格式化,目前只处理数组类型
    for(NSString *key in dic.allKeys)
    {
        id value=[dic objectForKey:key];
        JSON_KEY_STYLE style=[self checkStyle:value];
        if(style==JSON_KEY_STYLE_NSArray&&[value count]>0)
        {
            NSString *subClassName=[NSString stringWithFormat:@"%@_%@",className,[self toLowerCaseString:key]];
            [rootAtt appendFormat:@"\n%@",[self getBeanData:subClassName data:value[0]]];
        }
    }
    return rootAtt;
}
+(JSON_KEY_STYLE)checkStyle:(id)value
{
    if([value isKindOfClass:NSArray.class])
        return JSON_KEY_STYLE_NSArray;
    if([value isKindOfClass:NSDictionary.class])
        return JSON_KEY_STYLE_NSDictionary;
    if([value isKindOfClass:NSString.class])
        return JSON_KEY_STYLE_NSString;
    //介于bool跟int之间无法识别,这里就不进行具体细分了
    //    if([value isKindOfClass:NSClassFromString(@"NSCFNumber").class])
    //        return JSON_KEY_STYLE_NSString;
    //    NSString *valueStr=value;
    //    const char *value_char=[@([valueStr integerValue]).stringValue cStringUsingEncoding:NSUTF8StringEncoding];
    //    if(strcasecmp(value_char, @encode(BOOL)))
    //        return JSON_KEY_STYLE_BOOL;
    //    if(strcasecmp(value_char, @encode(NSInteger)))
    //        return JSON_KEY_STYLE_NSInteger;
    //    if(strcasecmp(value_char, @encode(CGFloat)))
    //        return JSON_KEY_STYLE_CGFloat;
    return JSON_KEY_STYLE_Defaults;
}

/**
 * 根据输入的type类型转换成对应的输出
 *
 * @param type 
 *           0 - NSString
 *           1 - NSInteger
 *           2 - CGFloat
 *           3 - BOOL
 *           4 - NSArray
 *           5 - NSDictionary
 *
 * @return .h文件类型定义
 */
+(NSString *)attribute:(JSON_KEY_STYLE)style t:(NSString *)t
{
    NSMutableString *att=[NSMutableString new];
    [att appendString:@"@property (nonatomic,readonly)"];
    switch (style) {
        case JSON_KEY_STYLE_NSString:
            [att appendString:@"NSString *"];
            break;
        case JSON_KEY_STYLE_NSInteger:
            [att appendString:@"NSInteger "];
            break;
        case JSON_KEY_STYLE_CGFloat:
            [att appendString:@"CGFloat "];
            break;
        case JSON_KEY_STYLE_BOOL:
            [att appendString:@"BOOL "];
            break;
        case JSON_KEY_STYLE_NSArray:
            [att appendString:@"NSArray *"];
            break;
        case JSON_KEY_STYLE_NSDictionary:
            [att appendString:@"NSDictionary *"];
            break;
        default:
            [att appendString:@"id "];
            break;
    }
    [att appendFormat:@"\t%@;",[self toLowerCaseString:t]];
    return att;
}
+(NSString *)toLowerCaseString:(NSString *)s
{
    return [s stringByReplacingCharactersInRange:NSMakeRange(0, 1) withString:[[s substringToIndex:1]lowercaseString]];
}
/**
 * 根据输入的type类型转换成对应的输出
 *
 * @param type
 *           0 - NSString
 *           1 - NSInteger
 *           2 - CGFloat
 *           3 - BOOL
 *           4 - NSArray
 *           5 - NSDictionary
 *
 * @param t 当前的key
 * @param className 用于数组生成名称
 *
 * @return .h文件类型定义
 */
+(NSString *)example:(JSON_KEY_STYLE)style t:(NSString *)t c:(NSString *)className
{
    NSString *los=[self toLowerCaseString:t];
    switch (style)
    {
        case JSON_KEY_STYLE_NSString:
            return [NSString stringWithFormat:@"-(NSString *)%@\n{\n\t return self.m_Dictionary[@\"%@\"];\n}\n",los,t];
        case JSON_KEY_STYLE_NSInteger:
            return [NSString stringWithFormat:@"-(NSInteger)%@\n{\n\t return [self.m_Dictionary[@\"%@\"]integerValue];\n}\n",los,t];
        case JSON_KEY_STYLE_CGFloat:
            return [NSString stringWithFormat:@"-(CGFloat)%@\n{\n\t return [self.m_Dictionary[@\"%@\"]floatValue];\n}\n",los,t];
        case JSON_KEY_STYLE_BOOL:
            return [NSString stringWithFormat:@"-(BOOL)%@\n{\n\t return [self.m_Dictionary[@\"%@\"]boolValue];\n}\n",los,t];
        case JSON_KEY_STYLE_NSArray:
            return [NSString stringWithFormat:@"-(NSArray *)%@\n{\n\t return toArray(self.m_Dictionary[@\"%@\"], ^id(NSDictionary *dic){\n\t\t return [%@ JsonObject:dic];\n\t});\n}",los,t,className];
        case JSON_KEY_STYLE_NSDictionary:
            return [NSString stringWithFormat:@"-(NSDictionary *)%@\n{\n\t return self.m_Dictionary[@\"%@\"];\n}\n",los,t];
        default:
            return [NSString stringWithFormat:@"-(id)%@\n{\n\t return self.m_Dictionary[@\"%@\"];\n}\n",los,t];
    }
}

@end

